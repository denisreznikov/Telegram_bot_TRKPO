import ast

import pytest
import urllib3
from test.integr.safe import token, url
from test.integr.telegramClient import TelegramClient

client = TelegramClient(botToken=token, url=url)
def test_start_end():
    urllib3.disable_warnings()
    mes = client.makeMessage("/start")
    client.sendCommand(mes)
    resp = client.getUpdates(False)

    text = resp['result'][0]['message']['text']
    assert text.startswith('Привет,')
    mes = client.makeMessage("/end")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Пока,')

def test_geoposition_cafe():
    urllib3.disable_warnings()
    mes = client.makeMessage("/start")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    text.startswith('Привет,')

    mes = client.makeMessage("/eat")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    text.startswith('Выберети,')

    keyboard = ast.literal_eval(resp['result'][0]['message']['reply_markup'])['inline_keyboard']
    mes = client.makeCallback(keyboard[0][0]["callback_data"])
    client.sendCallback(mes)
    mes = client.makeMessage("")
    client.sendLocation(mes, 30, 50)
    resp = client.getUpdates(False)
    length = len(resp['result'])
    text = resp['result'][length-1]['message']['text']
    assert text.startswith('Увеличить')
    mes = client.makeMessage("/end")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Пока,')

def test_geoposition_cafe_full():
    urllib3.disable_warnings()
    mes = client.makeMessage("/start")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    text.startswith('Привет,')

    mes = client.makeMessage("/eat")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    text.startswith('Выберети,')

    keyboard = ast.literal_eval(resp['result'][0]['message']['reply_markup'])['inline_keyboard']
    mes = client.makeCallback(keyboard[0][0]["callback_data"])
    client.sendCallback(mes)
    mes = client.makeMessage("")
    client.sendLocation(mes, 30, 50)
    resp = client.getUpdates(False)
    length = len(resp['result'])
    text = resp['result'][length-1]['message']['text']
    assert text.startswith('Увеличить')

    mes = client.makeMessage("/more")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    length = len(resp['result'])
    text = resp['result'][length-1]['message']['text']
    assert text.startswith('Пока')

def test_geoposition_cafe_full_negative():
    urllib3.disable_warnings()
    mes = client.makeMessage("/start")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    text.startswith('Привет,')

    mes = client.makeMessage("/eat")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    text.startswith('Выберети,')

    keyboard = ast.literal_eval(resp['result'][0]['message']['reply_markup'])['inline_keyboard']
    mes = client.makeCallback(keyboard[0][0]["callback_data"])
    client.sendCallback(mes)
    mes = client.makeMessage("")
    client.sendLocation(mes, 0, 0)
    resp = client.getUpdates(False)
    length = len(resp['result'])
    text = resp['result'][length-1]['message']['text']
    assert text.startswith('Увеличить')

    mes = client.makeMessage("/more")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    length = len(resp['result'])
    text = resp['result'][length-1]['message']['text']
    assert text.startswith('Api yandex')

def test_weather_city():
    urllib3.disable_warnings()
    mes = client.makeMessage("/start")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Привет,')

    mes = client.makeMessage("/weather")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Введите')

    mes = client.makeMessage("Москва")
    client.sendMessage(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('conditions')

    mes = client.makeMessage("/end")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Пока,')

def test_weather_city_negative():
    urllib3.disable_warnings()
    mes = client.makeMessage("/start")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Привет,')

    mes = client.makeMessage("/weather")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Введите')

    mes = client.makeMessage("Test_city_name")
    client.sendMessage(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Я не знаю')

    mes = client.makeMessage("/end")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Пока,')

def test_weather_city_full():
    urllib3.disable_warnings()
    mes = client.makeMessage("/start")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Привет,')

    mes = client.makeMessage("/weather")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Введите')

    mes = client.makeMessage("Москва")
    client.sendMessage(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('conditions')

    keyboard = ast.literal_eval(resp['result'][1]['message']['reply_markup'])['inline_keyboard']
    mes = client.makeCallback(keyboard[0][0]["callback_data"])
    client.sendCallback(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('')

def test_weather_location():
    urllib3.disable_warnings()
    mes = client.makeMessage("/start")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Привет,')

    mes = client.makeMessage("/weather")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Введите')

    mes = client.makeMessage("")
    client.sendLocation(mes, 30, 50)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('conditions')

    mes = client.makeMessage("/end")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Пока,')

def test_weather_location_full():
    urllib3.disable_warnings()
    mes = client.makeMessage("/start")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Привет,')

    mes = client.makeMessage("/weather")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Введите')

    mes = client.makeMessage("")
    client.sendLocation(mes, 30, 50)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('conditions')

    keyboard = ast.literal_eval(resp['result'][0]['message']['reply_markup'])['inline_keyboard']
    mes = client.makeCallback(keyboard[0][0]["callback_data"])
    client.sendCallback(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('')

def test_avito():
    urllib3.disable_warnings()
    mes = client.makeMessage("/start")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Привет,')

    mes = client.makeMessage("/avito")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Напишите')

    mes = client.makeMessage("BMW")
    client.sendMessage(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Теперь')

    mes = client.makeMessage("Девяткино")
    client.sendMessage(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Теперь')

    keyboard = ast.literal_eval(resp['result'][0]['message']['reply_markup'])['inline_keyboard']
    mes = client.makeCallback(keyboard[0][0]["callback_data"])
    client.sendCallback(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('searching')
    mes = client.makeMessage("/end")
    client.sendCommand(mes)
    client.getUpdates(True)

def test_avito_negative():
    urllib3.disable_warnings()
    mes = client.makeMessage("/start")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Привет,')

    mes = client.makeMessage("/avito")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Напишите')

    mes = client.makeMessage("BMW")
    client.sendMessage(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Теперь')

    mes = client.makeMessage("Тестинская")
    client.sendMessage(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Такого метро')

    mes = client.makeMessage("/end")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Пока,')

