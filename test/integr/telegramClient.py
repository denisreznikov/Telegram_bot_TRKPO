import ast
import math
from datetime import datetime

import requests
import urllib3
from telegram import Location

from test.integr.safe import url, token

urllib3.disable_warnings()


class TelegramClient:
    def __init__(
        self,
        botToken: str = None,
        url: str = None
    ):
        super().__init__()
        self.botToken = botToken
        self.url = url
    def makeMessage(self,messageText):
        date = int(math.floor(datetime.timestamp(datetime.today())))
        message = {
            'message_id': 1,
            'chat': {'id': 0, 'first_name': "Vlad", 'type': "private"},
            "date": date,
            "from": {"id": 0, "first_name": "Vlad", "is_bot": "False"},
            "text": messageText,
            "entities": [
                {"type": "bot_command", "offset": 0, "length": len(messageText)}
            ],
            "botToken": self.botToken
        }
        return message
    def makeCallback(self,callbackData):
        #respMes = resp['result'][0]['message']
        #respMes["message_id"] = resp['result'][0]['messageId']
#
        #time = resp['result'][0]['time']
        #timestamp = datetime.fromtimestamp(time/ 1e3)
        #date = int(math.floor(datetime.timestamp(timestamp)))
        #respMes["date"] = date
        date = int(math.floor(datetime.timestamp(datetime.today())))
        callbackQuery = {
            "chat": {'id': 0, 'first_name': "Test Name", 'type': "private"},

            "from": {"id": 0, "first_name": "user", "is_bot": "False"},
            "message": {
                'message_id': 0,
                'chat': {'id': 0, 'first_name': "Vlad", 'type': "private"},
                "date": date,
                "from": {"id": 0, "first_name": "Vlad", "is_bot": "False"},
                "botToken": self.botToken
            },

            "data": callbackData,
            'chat_instance': "5",
            "botToken": self.botToken
        }
        return callbackQuery
    def sendMessage(self,message):
        requestUrl = url + "/sendMessage"
        print(requestUrl)
        responce = requests.post(requestUrl, json = message, verify=False).json()
        return responce
    def sendCommand(self,message):
        requestUrl = url + "/sendCommand"
        print(requestUrl)
        responce = requests.post(requestUrl, json = message, verify=False).json()
        return responce
    def sendCallback(self,message):
        requestUrl = url + "/sendCallback"
        print(requestUrl)
        responce = requests.post(requestUrl, json = message, verify=False).json()
        return responce
    def sendLocation(self, message,longitude, latitude):
        requestUrl = url + "/sendMessage"
        message["location"] = dict(longitude=longitude, latitude=latitude)
        print(requestUrl)
        responce = requests.post(requestUrl, json = message, verify=False).json()
        return responce
    def getUpdates(self, nullResponce):
        requestUrl = url + "/getUpdates"
        print(requestUrl)
        failCount = 0
        while True:
            updates = requests.post(requestUrl, data = {"token": token}, verify=False).json()
            if ((len(updates['result'] ) > 0) | nullResponce == True):
                break
            if (failCount > 4):
                raise Exception
            failCount += 1
        return updates

client = TelegramClient(botToken=token, url=url)
def test_a():
    urllib3.disable_warnings()
    mes = client.makeMessage("/start")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Привет,')

    mes = client.makeMessage("/weather")
    client.sendCommand(mes)
    resp = client.getUpdates()
    text = resp['result'][0]['message']['text']
    assert text.startswith('')

    mes = client.makeMessage("")
    client.sendLocation(mes, 30, 50)
    resp = client.getUpdates()
    text = resp['result'][0]['message']['text']
    print(text)

    mes = client.makeMessage("/end")
    client.sendCommand(mes)
    resp = client.getUpdates()
    text = resp['result'][0]['message']['text']
    assert text.startswith('Пока,')

def test_b():
    urllib3.disable_warnings()
    mes = client.makeMessage("/start")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Привет,')

    mes = client.makeMessage("/weather")
    client.sendCommand(mes)
    resp = client.getUpdates()
    text = resp['result'][0]['message']['text']
    assert text.startswith('')

    mes = client.makeMessage("Москва")
    client.sendMessage(mes)
    resp = client.getUpdates()
    text = resp['result'][0]['message']['text']
    assert text.startswith('')
    keyboard = ast.literal_eval(resp['result'][1]['message']['reply_markup'])['inline_keyboard']
    mes = client.makeCallback(keyboard[0][0]["callback_data"])
    client.sendCallback(mes)
    resp = client.getUpdates()
    text = resp['result'][0]['message']['text']
    print(text)
def test_c():
    urllib3.disable_warnings()
    mes = client.makeMessage("/start")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    text.startswith('Привет,')

    mes = client.makeMessage("/eat")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    text.startswith('Выберети,')

    keyboard = ast.literal_eval(resp['result'][0]['message']['reply_markup'])['inline_keyboard']

    mes = client.makeCallback(keyboard[0][0]["callback_data"])
    client.sendCallback(mes)
    mes = client.makeMessage("")
    client.sendLocation(mes, 30, 50)
    resp = client.getUpdates(False)
    length = len(resp['result'])

    text = resp['result'][length-1]['message']['text']
    #while True:
    #    if (text.startswith('Увеличить')):
    #        break
    #    resp = client.getUpdates()
    #    text = resp['result'][0]['message']['text']
    #    print(text)


    assert text.startswith('Увеличить')
    mes = client.makeMessage("/end")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Пока,')
def test_d():
    urllib3.disable_warnings()
    mes = client.makeMessage("/start")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Привет,')

    mes = client.makeMessage("/avito")
    client.sendCommand(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Напишите')

    mes = client.makeMessage("BMW")
    client.sendMessage(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Теперь')

    mes = client.makeMessage("Девяткино")
    client.sendMessage(mes)
    resp = client.getUpdates(False)
    text = resp['result'][0]['message']['text']
    assert text.startswith('Теперь')

    keyboard = ast.literal_eval(resp['result'][0]['message']['reply_markup'])['inline_keyboard']
    mes = client.makeCallback(keyboard[0][0]["callback_data"])
    client.sendCallback(mes)
    resp = client.getUpdates()
    text = resp['result'][0]['message']['text']
    assert text.startswith('')
#test_d()


