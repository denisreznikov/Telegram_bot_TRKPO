from pytest import mark
from telethon import TelegramClient
from telethon.tl.custom.message import Message

#client = TelegramClient('Vlad', api_id, api_hash)

@mark.asyncio
async def test_cafe(client: TelegramClient):
    async with client.conversation("@trpko_70115_bot", timeout=5) as conv:
        await conv.send_message("/start")
        resp: Message = await conv.get_response()
        start_resp = resp
        assert resp.raw_text.startswith('Привет,')
        await conv.send_message("/eat")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Выберети')
        await resp.click(text = "Cafe ☕ ")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Отправьте')
        await start_resp.click(text="Send location", share_geo=(30.35, 59.98))
        resp: Message = await conv.get_response()
        resp: Message = await conv.get_response()
        resp: Message = await conv.get_response()
        resp: Message = await conv.get_response()
        resp: Message = await conv.get_response()
        resp: Message = await conv.get_response()
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Увеличить')
        await conv.send_message("/end")
        resp: Message = await conv.get_response()
        print(resp.raw_text)
        assert resp.raw_text.startswith("Пока")

@mark.asyncio
async def test_cafe_more(client: TelegramClient):
    async with client.conversation("@trpko_70115_bot", timeout=5) as conv:
        await conv.send_message("/start")
        resp: Message = await conv.get_response()
        start_resp = resp
        assert resp.raw_text.startswith('Привет,')
        await conv.send_message("/eat")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Выберети')
        await resp.click(text = "Cafe ☕ ")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Отправьте')
        await start_resp.click(text="Send location", share_geo=(30.35, 59.98))
        resp: Message = await conv.get_response()
        resp: Message = await conv.get_response()
        resp: Message = await conv.get_response()
        resp: Message = await conv.get_response()
        resp: Message = await conv.get_response()
        resp: Message = await conv.get_response()
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Увеличить')
        await conv.send_message("/more")
        resp: Message = await conv.get_response()
        resp: Message = await conv.get_response()
        resp: Message = await conv.get_response()
        resp: Message = await conv.get_response()
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Пока')
@mark.asyncio
async def test_weather_city(client: TelegramClient):
    async with client.conversation("@trpko_70115_bot", timeout=5) as conv:
        await conv.send_message("/start")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Привет,')
        await conv.send_message("/weather")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Введите')
        await conv.send_message("Москва")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('conditions')
        resp: Message = await conv.get_response()
        await resp.click(text = "No  🙅‍♂ ")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Okay')

@mark.asyncio
async def test_weather_city_full(client: TelegramClient):
    async with client.conversation("@trpko_70115_bot", timeout=5) as conv:
        await conv.send_message("/start")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Привет,')
        await conv.send_message("/weather")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Введите')
        await conv.send_message("Москва")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('conditions')
        resp: Message = await conv.get_response()
        await resp.click(text = "Yes 🆗")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('2')

@mark.asyncio
async def test_weather_location(client: TelegramClient):
    async with client.conversation("@trpko_70115_bot", timeout=5) as conv:
        await conv.send_message("/start")
        resp: Message = await conv.get_response()
        start_resp = resp
        assert resp.raw_text.startswith('Привет,')
        await conv.send_message("/weather")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Введите')
        await start_resp.click(text="Send location", share_geo=(30.35, 59.98))
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('conditions')
        await resp.click(text = "No  🙅‍♂ ")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Okay')

@mark.asyncio
async def test_weather_location_full(client: TelegramClient):
    async with client.conversation("@trpko_70115_bot", timeout=5) as conv:
        await conv.send_message("/start")
        resp: Message = await conv.get_response()
        start_resp = resp
        assert resp.raw_text.startswith('Привет,')
        await conv.send_message("/weather")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Введите')
        await start_resp.click(text="Send location", share_geo=(30.35, 59.98))
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('conditions')
        await resp.click(text = "Yes 🆗")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('2')

@mark.asyncio
async def test_avito(client: TelegramClient):
    async with client.conversation("@trpko_70115_bot", timeout=5) as conv:
        await conv.send_message("/start")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Привет,')
        await conv.send_message("/avito")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Напишите')
        await conv.send_message("BMW")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Теперь напишите')
        await conv.send_message("Девяткино")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith('Теперь выберите')
        await resp.click(text = "Дешевые")
        resp: Message = await conv.get_response()
        assert resp.raw_text.startswith("searching")
        await conv.send_message("/end")


